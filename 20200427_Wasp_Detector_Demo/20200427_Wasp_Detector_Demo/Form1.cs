﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;

namespace _20200427_Wasp_Detector_Demo
{
    
    
    public partial class Form1 : Form
    {
        private Timer UncalibratedSamples_Timer;
        private Timer CalibratedSamples_Timer;
        public SerialPort _moonlightColourSensorSerial = new SerialPort();

        private bool led_enable = false;
        private int samples_to_take = 10;
        private int sample_counter = 0;

        private string latest_sample;



        public Form1()
        {
            InitializeComponent();
            getavaialbleports();
        }

        /* Check all the available serial ports and list them in 
         * the comboBox */
        void getavaialbleports()
        {
            String[] ports = SerialPort.GetPortNames();
            comboBox1.Items.AddRange(ports);
        }


        private void comboBox1_SelectedIndexChanged_1(object sender, EventArgs e)
        {

        }

        /* Connect to the chosen serial port, selected in the comboBox */
        private void Connect_Click(object sender, EventArgs e)
        {
            if (comboBox1.Text == "")
            {
                outputRichTextBox.AppendText("Please select a port from the drop down menu");
            }
            else
            {
                _moonlightColourSensorSerial.PortName = comboBox1.Text;
                _moonlightColourSensorSerial.BaudRate = 115200;
                _moonlightColourSensorSerial.Open();
                _moonlightColourSensorSerial.DataReceived += new SerialDataReceivedEventHandler(serial_DataReceived);
                Connect.Enabled = false;
                LED_control.Enabled = true;
            }
        }

        /* Event for handling data recieved over the serial port
         * Currently unused */
        public void serial_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            SerialPort sp = (SerialPort)sender;

            //richTextBox1.AppendText(sp.ReadExisting() + "\n");
            //richTextBox2.AppendText("Hello" + "\n");
        }

        /* Timer tick event for the uncalibrated sample timer.
         * On a tick, read an uncalibrated data sample from the sensors
         * and display it in the output text box. When the specified number
         * of samples has been reached, turn the timer off and stop taking
         * samples. */
        public void UncalibratedSamples_Tick(object sender, EventArgs e)
        {
            if (_moonlightColourSensorSerial.IsOpen)
            {
                _moonlightColourSensorSerial.WriteLine("ATDATA");

                latest_sample = _moonlightColourSensorSerial.ReadLine();

                //outputRichTextBox.AppendText(_moonlightColourSensorSerial.ReadLine());
                outputRichTextBox.AppendText(latest_sample);
                outputRichTextBox.AppendText("\n");
                outputRichTextBox.AppendText("\n");

            }
            else
            {
                outputRichTextBox.AppendText("Please select a serial port");
                outputRichTextBox.AppendText("\n");
                UncalibratedSamples_Timer.Enabled = false;
                sample_counter = 0;
            }

            sample_counter++;

            if (sample_counter == samples_to_take)
            {
                UncalibratedSamples_Timer.Enabled = false;
                sample_counter = 0;
            }
        }

        /* Timer tick event for the calibrated sample timer.
         * On a tick, read an uncalibrated data sample from the sensors
         * and display it in the output text box. When the specified number
         * of samples has been reached, turn the timer off and stop taking
         * samples. */
        public void CalibratedSamples_Tick(object sender, EventArgs e)
        {
            if (_moonlightColourSensorSerial.IsOpen)
            {
                _moonlightColourSensorSerial.WriteLine("ATCDATA");

                latest_sample = _moonlightColourSensorSerial.ReadLine();

                //outputRichTextBox.AppendText(_moonlightColourSensorSerial.ReadLine());
                outputRichTextBox.AppendText(latest_sample);
                outputRichTextBox.AppendText("\n");

            }
            else
            {
                outputRichTextBox.AppendText("Please select a serial port");
                outputRichTextBox.AppendText("\n");
                CalibratedSamples_Timer.Enabled = false;
                sample_counter = 0;
            }

            sample_counter++;

            if (sample_counter == samples_to_take)
            {
                CalibratedSamples_Timer.Enabled = false;
                sample_counter = 0;
            }
        }

        /* Take one uncalibrated sample on a button click */
        private void UncalSample_Click(object sender, EventArgs e)
        {
            if (_moonlightColourSensorSerial.IsOpen)
            {
                _moonlightColourSensorSerial.WriteLine("ATDATA");

                latest_sample = _moonlightColourSensorSerial.ReadLine();

                //outputRichTextBox.AppendText(_moonlightColourSensorSerial.ReadLine());
                outputRichTextBox.AppendText(latest_sample);
                outputRichTextBox.AppendText("\n");

                LatestColourSample.Enabled = true; // Enable graphing
            }
            else
            {
                outputRichTextBox.AppendText("Please select a serial port");
                outputRichTextBox.AppendText("\n");
            }
        }

        /* Take one calibrated sample on a button click */
        private void CalSample_Click(object sender, EventArgs e)
        {
            if (_moonlightColourSensorSerial.IsOpen)
            {
                _moonlightColourSensorSerial.WriteLine("ATCDATA");

                latest_sample = _moonlightColourSensorSerial.ReadLine();

                //outputRichTextBox.AppendText(_moonlightColourSensorSerial.ReadLine());
                outputRichTextBox.AppendText(latest_sample);
                outputRichTextBox.AppendText("\n");

                LatestColourSample.Enabled = true; // Enable graphing
            }
            else
            {
                outputRichTextBox.AppendText("Please select a serial port");
                outputRichTextBox.AppendText("\n");
            }
        }

        /* Send the message typed into the input text box to the 
         * AMS sensors over serial
         */
        private void Send_Click(object sender, EventArgs e)
        {
            
            if (_moonlightColourSensorSerial.IsOpen && inputRichTextBox.Text != "")
            {
                _moonlightColourSensorSerial.WriteLine(inputRichTextBox.Text);

                outputRichTextBox.AppendText(_moonlightColourSensorSerial.ReadLine());
                outputRichTextBox.AppendText("\n");

                
            }
            else if (inputRichTextBox.Text == "")
            {
                // do nothing
            }
            else
            {
                outputRichTextBox.AppendText("Please select a serial port");
                outputRichTextBox.AppendText("\n");
            }
        }

        /* On a button click, take Z amount of uncalibrated samples 
         through use of a timer */
        private void UnCalSample10_Click(object sender, EventArgs e)
        {

            //Setup our Timer
            UncalibratedSamples_Timer = new Timer();
            UncalibratedSamples_Timer.Tick += new EventHandler(UncalibratedSamples_Tick);
            UncalibratedSamples_Timer.Interval = 500; //in ms
            UncalibratedSamples_Timer.Enabled = true;  //Start our timer

            LatestColourSample.Enabled = true; // Enable graphing

        }

        /* On a button click, take Z amount of calibrated samples 
         through use of a timer */
        private void CalibratedSampleZ_Click(object sender, EventArgs e)
        {

            //Setup our Timer
            CalibratedSamples_Timer = new Timer();
            CalibratedSamples_Timer.Tick += new EventHandler(CalibratedSamples_Tick);
            CalibratedSamples_Timer.Interval = 500; //in ms
            CalibratedSamples_Timer.Enabled = true;  //Start our timer

            LatestColourSample.Enabled = true; // Enable graphing

        }

        /* When the user types in a new number, change the 'samples_to_take' variable
         * to the specified number
         * Default to 10 samples */
        private void NumberSamplesRichTextBox_TextChanged(object sender, EventArgs e)
        {
            if (Int32.TryParse(NumberSamplesRichTextBox.Text, out samples_to_take))
            {
                // you know that the parsing attempt
                // was successful
            }
            else if (NumberSamplesRichTextBox.Text == "")
            {
                samples_to_take = 10;
            }
            else
            {
                outputRichTextBox.AppendText("Please enter a valid number");
                outputRichTextBox.AppendText("\n");
            }
        }

        /* On a button click, set all three drver LEDs to be on at 12.5 mA */
        private void LED_control_Click(object sender, EventArgs e)
        {
            if (!led_enable)
            {
                LED_control.Text = "Turn LEDs Off";

                // 12.5 mA
                _moonlightColourSensorSerial.WriteLine("ATLEDC=0");
                _moonlightColourSensorSerial.ReadLine();

                // LED1 
                _moonlightColourSensorSerial.WriteLine("ATLED1=1");
                _moonlightColourSensorSerial.ReadLine();


                _moonlightColourSensorSerial.WriteLine("ATLEDD=0");
                _moonlightColourSensorSerial.ReadLine();

                _moonlightColourSensorSerial.WriteLine("ATLED3=1");
                _moonlightColourSensorSerial.ReadLine();


                _moonlightColourSensorSerial.WriteLine("ATLEDE=0");
                _moonlightColourSensorSerial.ReadLine();

                _moonlightColourSensorSerial.WriteLine("ATLED5=1");
                _moonlightColourSensorSerial.ReadLine();

                led_enable = true;
            }
            else
            {
                LED_control.Text = "Turn LEDs on";

                _moonlightColourSensorSerial.WriteLine("ATLED1=0");
                _moonlightColourSensorSerial.ReadLine();

                _moonlightColourSensorSerial.WriteLine("ATLED3=0");
                _moonlightColourSensorSerial.ReadLine();

                _moonlightColourSensorSerial.WriteLine("ATLED5=0");
                _moonlightColourSensorSerial.ReadLine();

                led_enable = false;
            }
        }

        private void LatestColourSample_Click(object sender, EventArgs e)
        {
            string [] split_samples = latest_sample.Split(' ');
            string temp;
            int index = 0;
            double[] samples_18 = new double[18];

            foreach (string s in split_samples)
            {
                temp = s.Trim(new Char[] { ' ', ',' });
                samples_18[index] = double.Parse(temp);
                index++;
                if (index == 18)
                {
                    break;
                }
            }

            LatestColourSample.Series[0].Points.Clear();
            LatestColourSample.Series[0].Points.AddXY("R(610nm)", samples_18[0]);
            LatestColourSample.Series[0].Points.AddXY("S(680nm)", samples_18[1]);
            LatestColourSample.Series[0].Points.AddXY("T(730nm)", samples_18[2]);
            LatestColourSample.Series[0].Points.AddXY("U(760nm)", samples_18[3]);
            LatestColourSample.Series[0].Points.AddXY("V(810nm)", samples_18[4]);
            LatestColourSample.Series[0].Points.AddXY("W(860nm)", samples_18[5]);
            LatestColourSample.Series[0].Points.AddXY("G(560nm)", samples_18[6]);
            LatestColourSample.Series[0].Points.AddXY("H(585nm)", samples_18[7]);
            LatestColourSample.Series[0].Points.AddXY("I(645nm)", samples_18[8]);
            LatestColourSample.Series[0].Points.AddXY("J(705nm)", samples_18[9]);
            LatestColourSample.Series[0].Points.AddXY("K(900nm)", samples_18[10]);
            LatestColourSample.Series[0].Points.AddXY("L(940nm)", samples_18[11]);
            LatestColourSample.Series[0].Points.AddXY("A(410nm)", samples_18[12]);
            LatestColourSample.Series[0].Points.AddXY("B(435nm)", samples_18[13]);
            LatestColourSample.Series[0].Points.AddXY("C(460nm)", samples_18[14]);
            LatestColourSample.Series[0].Points.AddXY("D(485nm)", samples_18[15]);
            LatestColourSample.Series[0].Points.AddXY("E(510nm)", samples_18[16]);
            LatestColourSample.Series[0].Points.AddXY("F(535nm)", samples_18[17]);
            LatestColourSample.ChartAreas[0].AxisX.LabelStyle.Angle = -90;
        }

    }
}

