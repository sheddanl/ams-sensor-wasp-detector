﻿namespace _20200427_Wasp_Detector_Demo
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.Sample = new System.Windows.Forms.Button();
            this.inputRichTextBox = new System.Windows.Forms.RichTextBox();
            this.Connect = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.outputRichTextBox = new System.Windows.Forms.RichTextBox();
            this.UncalSample = new System.Windows.Forms.Button();
            this.UnCalSample10 = new System.Windows.Forms.Button();
            this.NumberSamplesRichTextBox = new System.Windows.Forms.RichTextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.LED_control = new System.Windows.Forms.Button();
            this.CalSample = new System.Windows.Forms.Button();
            this.CalibratedSampleZ = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.LatestColourSample = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.textBox3 = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.LatestColourSample)).BeginInit();
            this.SuspendLayout();
            // 
            // Sample
            // 
            this.Sample.Location = new System.Drawing.Point(252, 140);
            this.Sample.Name = "Sample";
            this.Sample.Size = new System.Drawing.Size(215, 23);
            this.Sample.TabIndex = 1;
            this.Sample.Text = "Send";
            this.Sample.UseVisualStyleBackColor = true;
            this.Sample.Click += new System.EventHandler(this.Send_Click);
            // 
            // inputRichTextBox
            // 
            this.inputRichTextBox.Location = new System.Drawing.Point(12, 62);
            this.inputRichTextBox.Name = "inputRichTextBox";
            this.inputRichTextBox.Size = new System.Drawing.Size(234, 101);
            this.inputRichTextBox.TabIndex = 6;
            this.inputRichTextBox.Text = "";
            // 
            // Connect
            // 
            this.Connect.Location = new System.Drawing.Point(132, 12);
            this.Connect.Name = "Connect";
            this.Connect.Size = new System.Drawing.Size(114, 23);
            this.Connect.TabIndex = 2;
            this.Connect.Text = "Connect";
            this.Connect.UseVisualStyleBackColor = true;
            this.Connect.Click += new System.EventHandler(this.Connect_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(12, 12);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(114, 21);
            this.comboBox1.TabIndex = 1;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged_1);
            // 
            // outputRichTextBox
            // 
            this.outputRichTextBox.Location = new System.Drawing.Point(12, 169);
            this.outputRichTextBox.Name = "outputRichTextBox";
            this.outputRichTextBox.Size = new System.Drawing.Size(455, 101);
            this.outputRichTextBox.TabIndex = 3;
            this.outputRichTextBox.Text = "";
            // 
            // UncalSample
            // 
            this.UncalSample.Location = new System.Drawing.Point(252, 111);
            this.UncalSample.Name = "UncalSample";
            this.UncalSample.Size = new System.Drawing.Size(96, 23);
            this.UncalSample.TabIndex = 7;
            this.UncalSample.Text = "Uncal. Sample";
            this.UncalSample.UseVisualStyleBackColor = true;
            this.UncalSample.Click += new System.EventHandler(this.UncalSample_Click);
            // 
            // UnCalSample10
            // 
            this.UnCalSample10.Location = new System.Drawing.Point(354, 111);
            this.UnCalSample10.Name = "UnCalSample10";
            this.UnCalSample10.Size = new System.Drawing.Size(113, 23);
            this.UnCalSample10.TabIndex = 8;
            this.UnCalSample10.Text = "Un Cal. Sample x Z";
            this.UnCalSample10.UseVisualStyleBackColor = true;
            this.UnCalSample10.Click += new System.EventHandler(this.UnCalSample10_Click);
            // 
            // NumberSamplesRichTextBox
            // 
            this.NumberSamplesRichTextBox.Location = new System.Drawing.Point(354, 62);
            this.NumberSamplesRichTextBox.Name = "NumberSamplesRichTextBox";
            this.NumberSamplesRichTextBox.Size = new System.Drawing.Size(113, 19);
            this.NumberSamplesRichTextBox.TabIndex = 9;
            this.NumberSamplesRichTextBox.Text = "10";
            this.NumberSamplesRichTextBox.TextChanged += new System.EventHandler(this.NumberSamplesRichTextBox_TextChanged);
            // 
            // textBox1
            // 
            this.textBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Location = new System.Drawing.Point(354, 43);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(113, 13);
            this.textBox1.TabIndex = 10;
            this.textBox1.Text = "Samples to Take (Z)";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // LED_control
            // 
            this.LED_control.Enabled = false;
            this.LED_control.Location = new System.Drawing.Point(573, 39);
            this.LED_control.Name = "LED_control";
            this.LED_control.Size = new System.Drawing.Size(215, 23);
            this.LED_control.TabIndex = 11;
            this.LED_control.Text = "LEDs On";
            this.LED_control.UseVisualStyleBackColor = true;
            this.LED_control.Click += new System.EventHandler(this.LED_control_Click);
            // 
            // CalSample
            // 
            this.CalSample.Location = new System.Drawing.Point(252, 82);
            this.CalSample.Name = "CalSample";
            this.CalSample.Size = new System.Drawing.Size(96, 23);
            this.CalSample.TabIndex = 12;
            this.CalSample.Text = "Cal. Sample";
            this.CalSample.UseVisualStyleBackColor = true;
            this.CalSample.Click += new System.EventHandler(this.CalSample_Click);
            // 
            // CalibratedSampleZ
            // 
            this.CalibratedSampleZ.Location = new System.Drawing.Point(354, 82);
            this.CalibratedSampleZ.Name = "CalibratedSampleZ";
            this.CalibratedSampleZ.Size = new System.Drawing.Size(113, 23);
            this.CalibratedSampleZ.TabIndex = 13;
            this.CalibratedSampleZ.Text = "Cal. Sample x Z";
            this.CalibratedSampleZ.UseVisualStyleBackColor = true;
            this.CalibratedSampleZ.Click += new System.EventHandler(this.CalibratedSampleZ_Click);
            // 
            // textBox2
            // 
            this.textBox2.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox2.Location = new System.Drawing.Point(13, 43);
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(113, 13);
            this.textBox2.TabIndex = 14;
            this.textBox2.Text = "Send AT Commands";
            this.textBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // LatestColourSample
            // 
            chartArea1.Name = "ChartArea1";
            this.LatestColourSample.ChartAreas.Add(chartArea1);
            this.LatestColourSample.Enabled = false;
            legend1.Name = "Legend1";
            this.LatestColourSample.Legends.Add(legend1);
            this.LatestColourSample.Location = new System.Drawing.Point(12, 276);
            this.LatestColourSample.Name = "LatestColourSample";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            series1.Legend = "Legend1";
            series1.MarkerSize = 7;
            series1.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Circle;
            series1.Name = "Latest Sample";
            this.LatestColourSample.Series.Add(series1);
            this.LatestColourSample.Size = new System.Drawing.Size(776, 162);
            this.LatestColourSample.TabIndex = 15;
            this.LatestColourSample.Text = "Latest Colour Sample";
            this.LatestColourSample.Click += new System.EventHandler(this.LatestColourSample_Click);
            // 
            // textBox3
            // 
            this.textBox3.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.textBox3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox3.Location = new System.Drawing.Point(629, 257);
            this.textBox3.Name = "textBox3";
            this.textBox3.ReadOnly = true;
            this.textBox3.Size = new System.Drawing.Size(159, 13);
            this.textBox3.TabIndex = 16;
            this.textBox3.Text = "Click graph to show latest sample";
            this.textBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.LatestColourSample);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.CalibratedSampleZ);
            this.Controls.Add(this.CalSample);
            this.Controls.Add(this.LED_control);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.NumberSamplesRichTextBox);
            this.Controls.Add(this.UnCalSample10);
            this.Controls.Add(this.UncalSample);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.outputRichTextBox);
            this.Controls.Add(this.Connect);
            this.Controls.Add(this.Sample);
            this.Controls.Add(this.inputRichTextBox);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.LatestColourSample)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Sample;
        private System.Windows.Forms.RichTextBox inputRichTextBox;
        private System.Windows.Forms.Button Connect;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.RichTextBox outputRichTextBox;
        private System.Windows.Forms.Button UncalSample;
        private System.Windows.Forms.Button UnCalSample10;
        private System.Windows.Forms.RichTextBox NumberSamplesRichTextBox;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button LED_control;
        private System.Windows.Forms.Button CalSample;
        private System.Windows.Forms.Button CalibratedSampleZ;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.DataVisualization.Charting.Chart LatestColourSample;
        private System.Windows.Forms.TextBox textBox3;
    }
}

